import { IEnvironment } from './environment.interface';

export const environment = {
  production: false,
  api_origin: 'http://dev.krasuna.com.ua/api',
};
