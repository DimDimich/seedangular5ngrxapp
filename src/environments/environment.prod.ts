import { IEnvironment } from './environment.interface';

export const environment: IEnvironment = {
  production: true,
  api_origin: 'http://krasuna.com.ua/api',
};
