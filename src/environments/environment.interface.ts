export interface IEnvironment {
  production: boolean;
  api_origin: string;
}
