import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuard } from './core/guards/auth';

const routes: Routes = [
  { path: 'admin', canLoad: [AuthGuard], loadChildren: 'app/admin/app-admin.module#AppAdminModule' },
  { path: '**', redirectTo: '' },
];

const config: ExtraOptions = { useHash: true };

@NgModule({
  providers: [AuthGuard],
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
