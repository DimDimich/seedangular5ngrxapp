import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Types } from './actions';
import { Router } from '@angular/router';
import { DefaultActionInterface } from '../common.models';
import { of } from 'rxjs/observable/of';

@Injectable()
export class Effects {
  constructor(private actions$: Actions, private router: Router) {}

  @Effect()
  routerEffects = this.actions$
  .ofType(Types.navigate, Types.pop)
  .mergeMap((action: DefaultActionInterface) => {

    this.router.navigate(action.args, action.payload);
    return of({
      type: action.resultType,
      args: action.args,
      payload: action.payload,
    });
  });

}
