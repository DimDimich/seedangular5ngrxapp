import { DefaultActionInterface } from '../common.models';
import { NavigationExtras, ActivatedRoute } from '@angular/router';

export const Types = {
  navigate: 'router.navigate',
  stash: 'router.stash',
  pop: 'router.pop',
}

export const ResultTypes = {
  navigate: 'router.navigateResult',
  stash: 'router.stashResult',
  pop: 'router.popResult',
}

export class Navigate implements DefaultActionInterface {
  readonly type = Types.navigate;
  readonly resultType = ResultTypes.navigate;
  constructor(public args: string[], public payload: NavigationExtras) {}
}

export class StashLocation implements DefaultActionInterface {
  readonly type = Types.stash;
  readonly resultType = ResultTypes.stash;
  public args = null;
  constructor(public payload: ActivatedRoute) {}
}

export class PopLocation implements DefaultActionInterface {
  readonly type = Types.pop;
  readonly resultType = ResultTypes.pop;
  public args = null;
  public payload = null;
  constructor() {}
}

