import { PaginationArgs } from '../common.models';

export interface CreatePayload {
  login: string;
  email: string;
  name: string;
  role: string;
  password: string;
}

export interface UpdatePayload {
  login: string;
  email: string;
  name: string;
  role: string;
  password: string;
}

export class UpdatePayloadImpl implements UpdatePayload {
  login: string;
  email: string;
  name: string;
  role: string;
  password: string;
  constructor() {
    this.login = '';
    this.email = '';
    this.name = '';
    this.role = '';
    this.password = '';
  }
}

export interface UpdateArgs {
  user_id: string;
}

export interface DeleteArgs {
  user_id: string;
}

export interface ForgotPasswordPayload {
  email: string;
}

export interface ConfirmPasswordPayload {
  password: string;
}

export interface FetchArgs extends PaginationArgs {
}

export interface FetchByFilterArgs extends PaginationArgs {
  login?: string;
  name?: string;
  email?: string;
}

export interface GetArgs {
  user_id: string;
}

export interface UserModel {
  id: string;
  name: string;
  login: string;
  email: string;
  waiting_email: string;
  phone: string;
  city: string;
  address: string;
  role: string;
  created: number;
  password?: string;
}

export type RoleModel = string;

export interface ResetPayload {
  selectedUser?: boolean;
  users?: boolean;
  roles?:  boolean;
  total?:  boolean;
}
