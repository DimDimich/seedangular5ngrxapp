import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UpdatePayloadImpl, UpdatePayload, UserModel } from './models';

export const generateForm = (seed: UserModel | UpdatePayload = new UpdatePayloadImpl()) => {
  return new FormGroup({
    login: new FormControl(seed.login, [Validators.required]),
    name: new FormControl(seed.name, [Validators.required]),
    email: new FormControl(seed.email, [Validators.required]),
    role: new FormControl(seed.role, [Validators.required]),
    password: new FormControl(seed.password, [Validators.required]),
  });
}
