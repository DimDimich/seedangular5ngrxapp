import { ResultTypes, ResetTypes } from './actions';
import { UserModel, RoleModel } from './models';
import { DefaultResultInterface } from '../common.models';

export interface State {
  selectedUser: UserModel;
  users: UserModel[];
  roles: RoleModel[];
  total: number;
}

export const initialState: State = {
  selectedUser: null,
  users: null,
  roles: null,
  total: 0,
};

export function reducer(state = initialState, action: DefaultResultInterface): State {

  switch(action.type) {
    case ResultTypes.create:
      return {
        ...state,
        selectedUser: action.payload[0],
        total: action.total,
      };
    case ResultTypes.get:
      return {
        ...state,
        selectedUser: action.payload[0],
        total: action.total,
      };
    case ResultTypes.fetch:
    case ResultTypes.fetchByFilter:
      return {
        ...state,
        users: action.payload,
        total: action.total,
      };
    case ResultTypes.roles:
      return {
        ...state,
        roles: action.payload,
      };
    case ResetTypes.resetField:
      Object.keys(action.payload).forEach(key => {
        if (action.payload[key]) {
          state[key] = null;
        }
      });
      return {...state};
  }

  return state;
}
