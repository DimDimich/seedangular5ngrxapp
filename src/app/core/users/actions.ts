import { DefaultActionInterface } from '../common.models';
import { CreatePayload, UpdatePayload, UpdateArgs, DeleteArgs, FetchArgs, GetArgs, ResetPayload, FetchByFilterArgs } from './models';

export const ResetTypes = {
  resetField: 'users.resetStoreField'
}

export const Types = {
  create: 'users.create',
  get: 'users.get',
  update: 'users.update',
  forgotPassword: 'users.forgotPassword',
  confirmPassword: 'users.confirmPassword',
  confirmEmail: 'users.confirmEmail',
  fetch: 'users.fetch',
  fetchByFilter: 'users.fetchByFilter',
  delete: 'users.delete',
  roles: 'users.roles',
}

export const ResultTypes = {
  create: 'users.createResult',
  get: 'users.getResult',
  update: 'users.updateResult',
  forgotPassword: 'users.forgotPasswordResult',
  confirmPassword: 'users.confirmPasswordResult',
  confirmEmail: 'users.confirmEmailResult',
  fetch: 'users.fetchResult',
  fetchByFilter: 'users.fetchByFilterResult',
  delete: 'users.deleteResult',
  roles: 'users.rolesResult',
}

export const ErrorTypes = {
  create: 'users.createError',
  get: 'users.getError',
  update: 'users.updateError',
  forgotPassword: 'users.forgotPasswordError',
  confirmPassword: 'users.confirmPasswordError',
  confirmEmail: 'users.confirmEmailError',
  fetch: 'users.fetchError',
  fetchByFilter: 'users.fetchByFilterError',
  delete: 'users.deleteError',
  roles: 'users.rolesError',
}

export class Create implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.create;
  readonly resultType = ResultTypes.create;
  readonly errorType = ErrorTypes.create;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public args = null;
  constructor(public payload: CreatePayload) {}
}

export class Update implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.update;
  readonly resultType = ResultTypes.update;
  readonly errorType = ErrorTypes.update;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  constructor(public args: UpdateArgs, public payload: UpdatePayload) {
    if (
      this.payload.password === null ||
      this.payload.password === undefined ||
      this.payload.password === ''
    ) {
      delete this.payload.password;
    }
  }
}

export class Delete implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.delete;
  readonly resultType = ResultTypes.delete;
  readonly errorType = ErrorTypes.delete;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: DeleteArgs) {}
}

export class Fetch implements DefaultActionInterface {
  readonly httpMethod = 'GET';
  readonly type = Types.fetch;
  readonly resultType = ResultTypes.fetch;
  readonly errorType = ErrorTypes.fetch;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: FetchArgs) {}
}

export class FetchByFilter implements DefaultActionInterface {
  readonly httpMethod = 'GET';
  readonly type = Types.fetchByFilter;
  readonly resultType = ResultTypes.fetchByFilter;
  readonly errorType = ErrorTypes.fetchByFilter;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: FetchByFilterArgs) {}
}

export class Get implements DefaultActionInterface {
  readonly httpMethod = 'GET';
  readonly type = Types.get;
  readonly resultType = ResultTypes.get;
  readonly errorType = ErrorTypes.get;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: GetArgs) {}
}

export class Roles implements DefaultActionInterface {
  readonly httpMethod = 'GET';
  readonly type = Types.roles;
  readonly resultType = ResultTypes.roles;
  readonly errorType = ErrorTypes.roles;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  public args = null;
  constructor() {}
}

export class ResetFields implements DefaultActionInterface {
  readonly type = ResetTypes.resetField;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public args = null;
  constructor(public payload: ResetPayload) {}
}
