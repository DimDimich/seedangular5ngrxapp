import { PaginationArgs } from '../common.models';

export interface CreatePayload {
  category_id: string;
  subcategory_id: string;
  article: string;
  sizes: string;
  name: string;
  description: string;
  lang: string;
}

export interface UpdateArgs {
  product_id: string;
}

export interface UpdatePayload {
  category_id: string;
  subcategory_id: string;
  article: string;
  sizes: string;
  name: string;
  description: string;
  lang: string;
}

export class UpdatePayloadImpl {
  category_id: string = null;
  subcategory_id: string = null;
  article: string;
  sizes: string = '';
  name: string = '';
  description: string = '';
  lang: string = '';
}

export interface DeleteArgs {
  product_id: string;
}

export interface GetArgs {
  product_id: string;
}

export interface FetchArgs extends PaginationArgs {
}

export interface FetchByFilterArgs extends PaginationArgs {
  q?: string;
  props_ids?: string;
  prefix?: 'yes' | 'no';
}

export interface FetchByFilterInputArgs extends PaginationArgs {
  q?: string;
  props_ids?: string[];
  prefix?: 'yes' | 'no';
}


export interface ProductModel {
  id: string;
  digest_id: string;
  category_id: string;
  subcategory_id: string;
  image_id: string;
  article: string;
  tags: string[];
  creation_time: number;
  category: string;
  subcategory: string;
  name: string;
  description: string;
  sizes: string;
  price: number;
  names: object;
  descriptions: object;
  url_image: string;
  url_image_large: string;
  url_image_small: string;
}

export interface ResetPayload {
  selectedProduct?: boolean;
  products?: boolean;
  total?:  boolean;
}
