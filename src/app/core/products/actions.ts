import { DefaultActionInterface } from '../common.models';
import {
  CreatePayload, DeleteArgs, UpdateArgs, UpdatePayload,
  GetArgs, FetchArgs, FetchByFilterArgs, FetchByFilterInputArgs,
  ResetPayload
} from './models';

export const ResetTypes = {
  resetField: 'products.resetStoreField'
}

export const Types = {
  create: 'products.create',
  update: 'products.update',
  updateTags: 'products.updateTags',
  get: 'products.get',
  multiget: 'products.multiget',
  delete: 'products.delete',
  fetch: 'products.fetch',
  fetchByFilter: 'products.fetchByFilter',
}

export const ResultTypes = {
  create: 'products.createResult',
  update: 'products.updateResult',
  updateTags: 'products.updateTagsResult',
  get: 'products.getResult',
  multiget: 'products.multigetResult',
  delete: 'products.deleteResult',
  fetch: 'products.fetchResult',
  fetchByFilter: 'products.fetchByFilterResult',
}

export const ErrorTypes = {
  create: 'products.createError',
  update: 'products.updateError',
  updateTags: 'products.updateTagsError',
  get: 'products.getError',
  multiget: 'products.multigetError',
  delete: 'products.deleteError',
  fetch: 'products.fetchError',
  fetchByFilter: 'products.fetchByFilterError',
}

export class Create implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.create;
  readonly resultType = ResultTypes.create;
  readonly errorType = ErrorTypes.create;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public args = null;
  constructor(public payload: CreatePayload) {}
}

export class Update implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.update;
  readonly resultType = ResultTypes.update;
  readonly errorType = ErrorTypes.update;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  constructor(public args: UpdateArgs, public payload: UpdatePayload) {}
}

export class Delete implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.delete;
  readonly resultType = ResultTypes.delete;
  readonly errorType = ErrorTypes.delete;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: DeleteArgs) {}
}

export class Get implements DefaultActionInterface {
  readonly httpMethod = 'GET';
  readonly type = Types.get;
  readonly resultType = ResultTypes.get;
  readonly errorType = ErrorTypes.get;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: GetArgs) {}
}

export class Fetch implements DefaultActionInterface {
  readonly httpMethod = 'GET';
  readonly type = Types.fetch;
  readonly resultType = ResultTypes.fetch;
  readonly errorType = ErrorTypes.fetch;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: FetchArgs) {}
}

export class FetchByFilter implements DefaultActionInterface {
  readonly httpMethod = 'GET';
  readonly type = Types.fetchByFilter;
  readonly resultType = ResultTypes.fetchByFilter;
  readonly errorType = ErrorTypes.fetchByFilter;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  public args: FetchByFilterArgs;
  constructor(args: FetchByFilterInputArgs) {
    this.args = {
      ...args,
      props_ids: args.props_ids ? args.props_ids.join(',') : '',
    };
  }
}

export class ResetFields implements DefaultActionInterface {
  readonly type = ResetTypes.resetField;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public args = null;
  constructor(public payload: ResetPayload) {}
}
