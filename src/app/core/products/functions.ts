import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UpdatePayloadImpl, UpdatePayload, ProductModel } from './models';

export const generateForm = (seed: ProductModel | UpdatePayload = new UpdatePayloadImpl()) => {
  return new FormGroup({
    name: new FormControl(seed.name, [Validators.required]),
  });
}
