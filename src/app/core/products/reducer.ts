import { ResultTypes, ResetTypes } from './actions';
import { DefaultResultInterface } from '../common.models';
import { ProductModel } from './models';

export interface State {
  selectedProduct: ProductModel;
  products: ProductModel[];
  total: number;
}

export const initialState: State = {
  selectedProduct: null,
  products: null,
  total: 0,
};

export function reducer(state = initialState, action: DefaultResultInterface): State {

  switch(action.type) {
    case ResultTypes.fetch:
      return {
        ...state,
        products: action.payload,
        total: action.total,
      };
    case ResetTypes.resetField:
      Object.keys(action.payload).forEach(key => {
        if (action.payload[key]) {
          state[key] = null;
        }
      });
      return {...state};
  }

  return state;
}
