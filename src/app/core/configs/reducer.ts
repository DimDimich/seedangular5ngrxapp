import { ResultTypes } from './actions';
import { DefaultResultInterface } from '../common.models';
import { ConfigModel } from './models';

export interface State {
  configs: ConfigModel;
}

export const initialState: State = {
  configs: null,
};

export function reducer(state = initialState, action: DefaultResultInterface): State {

  switch(action.type) {
    case ResultTypes.get:
      return {
        ...state,
        configs: action.payload[0]
      };
  }

  return state;
}
