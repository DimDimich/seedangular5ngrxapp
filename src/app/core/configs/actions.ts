import { DefaultActionInterface } from '../common.models';
import { UpdateArgs, UpdatePayload, GetArgs } from './models';

export const Types = {
  update: 'configs.update',
  get: 'configs.get',
}

export const ResultTypes = {
  update: 'configs.updateResult',
  get: 'configs.getResult',
}

export const ErrorTypes = {
  update: 'configs.updateError',
  get: 'configs.getError',
}

export class Update implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.update;
  readonly resultType = ResultTypes.update;
  readonly errorType = ErrorTypes.update;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  constructor(public args: UpdateArgs, public payload: UpdatePayload) {}
}

export class Get implements DefaultActionInterface {
  readonly httpMethod = 'GET';
  readonly type = Types.get;
  readonly resultType = ResultTypes.get;
  readonly errorType = ErrorTypes.get;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: GetArgs) {}
}
