export interface UpdatePayload {
  [key: string]: Config;
}

export interface UpdateArgs {
  config_id: string;
}

export interface GetArgs {
}

export interface ConfigModel {
  [key: string]: Config;
}

export interface Config {
  type?: 'number' | 'integer' | 'string' | 'string-multiline' | 'string-singleline';
  value?: string | number | boolean;
  pattern?: string;
  description?: { [key: string]: string; };
}
