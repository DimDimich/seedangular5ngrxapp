import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { DefaultActionInterface, DefaultResultInterface } from './common.models';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import { of } from 'rxjs/observable/of';

export interface HttpInterceptorInterface {
  request: (action: DefaultActionInterface) => any;
}

@Injectable()
export class HttpInterceptorImpl implements HttpInterceptorInterface {
  readonly defaultOptions = { withCredentials: true };

  private _busy: boolean = false;
  public get busy () {
    return this._busy;
  }

  public request = (action: DefaultActionInterface) => {
    this._busy = true;

    return this.http.request(action.httpMethod,
      `${environment.api_origin}/${action.type}`,
      { ...this.defaultOptions, params: action.args, body: action.payload }
    ).mergeMap((response: DefaultResultInterface) => {
      return this.processResponse(action, response);
    })
    .catch(e => {
      this._busy = false;
      return of(<DefaultResultInterface>{
        type: 'HTTP_ERROR',
        payload: e
      });
    });
  }

  constructor(public http: HttpClient) {}

  private processResponse(action: DefaultActionInterface, response: DefaultResultInterface) {
    this._busy = false;
    if (response.error) {
      response.type = action.errorType;
      return [response, ...action.actionsOnError];
    } else {
      response.type = action.resultType;
      return [response, ...action.actionsOnSuccess];
    }
  }
}
