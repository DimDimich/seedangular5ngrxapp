import { Types, ResultTypes } from './actions';
import { UserModel } from './models';
import { DefaultResultInterface } from '../common.models';

export interface State {
  user: UserModel;
  authorization: boolean;
  authorization_expires: number;
}

export const initialState: State = {
  user: null,
  authorization: false,
  authorization_expires: 0
};

export function reducer(state = initialState, action: DefaultResultInterface): State {

  switch(action.type) {
    case ResultTypes.login:
    case ResultTypes.me:
      return {
        ...state,
        user: action.payload[0]
      };
    case ResultTypes.logout:
      return {
        ...state,
        user: null
      };
  }

  return state;
}
