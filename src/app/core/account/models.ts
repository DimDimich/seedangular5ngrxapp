export interface LoginPayload {
  login: string;
  password: string;
}

export interface CreatePayload {
  login: string;
  email: string;
  name: string;
  role: string;
  password: string;
}

export interface UpdatePayload {
}

export interface BindEmailPayload {
}

export interface UpdatePasswordPayload {
}

export interface UserModel {
  id: string;
  name: string;
  login: string;
  email: string;
  waiting_email: string;
  phone: string;
  city: string;
  address: string;
  role: string;
  created: number;
}
