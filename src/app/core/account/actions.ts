import { DefaultActionInterface } from '../common.models';
import { CreatePayload, UpdatePayload, BindEmailPayload, LoginPayload, UpdatePasswordPayload } from './models';
import * as Router from '../../core/router';

export const Types = {
  create: 'account.create',
  login: 'account.login',
  me: 'account.me',
  logout: 'account.logout',
  logoutAll: 'account.logoutAll',
  update: 'account.update',
  changePassword: 'account.changePassword',
  bindEmail: 'account.bindEmail',
  delete: 'account.delete',
}

export const ResultTypes = {
  create: 'account.createResult',
  login: 'account.loginResult',
  me: 'account.meResult',
  logout: 'account.logoutResult',
  logoutAll: 'account.logoutAllResult',
  update: 'account.updateResult',
  changePassword: 'account.changePasswordResult',
  bindEmail: 'account.bindEmailResult',
  delete: 'account.deleteResult',
}

export const ErrorTypes = {
  create: 'account.createError',
  login: 'account.loginError',
  me: 'account.meError',
  logout: 'account.logoutError',
  logoutAll: 'account.logoutAllError',
  update: 'account.updateError',
  changePassword: 'account.changePasswordError',
  bindEmail: 'account.bindEmailError',
  delete: 'account.deleteError',
}

export class Login implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.login;
  readonly resultType = ResultTypes.login;
  readonly errorType = ErrorTypes.login;
  readonly actionsOnSuccess = [new Router.Navigate(['/admin/products'], {})];
  readonly actionsOnError = [];
  public args = null;
  constructor(public payload: LoginPayload) {}
}

export class Logout implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.logout;
  readonly resultType = ResultTypes.logout;
  readonly errorType = ErrorTypes.logout;
  public args = null;
  public payload = null;
  readonly actionsOnSuccess = [new Router.Navigate(['/admin/login'], {})];
  readonly actionsOnError = [];
  constructor() {}
}

export class LogoutAll implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.logoutAll;
  readonly resultType = ResultTypes.logoutAll;
  readonly errorType = ErrorTypes.logoutAll;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public args = null;
  public payload = null;
  constructor() {}
}

export class Me implements DefaultActionInterface {
  readonly httpMethod = 'GET';
  readonly type = Types.me;
  readonly resultType = ResultTypes.me;
  readonly errorType = ErrorTypes.me;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public args = null;
  public payload = null;
  constructor() {}
}

export class Create implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.create;
  readonly resultType = ResultTypes.create;
  readonly errorType = ErrorTypes.create;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public args = null;
  constructor(public payload: CreatePayload) {}
}

export class Update implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.update;
  readonly resultType = ResultTypes.update;
  readonly errorType = ErrorTypes.update;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public args = null;
  constructor(public payload: UpdatePayload) {}
}

export class ChangePassword implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.update;
  readonly resultType = ResultTypes.update;
  readonly errorType = ErrorTypes.update;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public args = null;
  constructor(public payload: UpdatePasswordPayload) {}
}

export class BindEmail implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.bindEmail;
  readonly resultType = ResultTypes.bindEmail;
  readonly errorType = ErrorTypes.bindEmail;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public args = null;
  constructor(public payload: BindEmailPayload) {}
}

export class Delete implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.create;
  readonly resultType = ResultTypes.create;
  readonly errorType = ErrorTypes.create;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public args = null;
  public payload = null;
  constructor() {}
}

