import { DefaultActionInterface } from '../common.models';
import { FetchArgs, CreatePayload, DeleteArgs } from './models';

export const Types = {
  create: 'feedbacks.create',
  delete: 'feedbacks.delete',
  fetch: 'feedbacks.fetch',
}

export const ResultTypes = {
  create: 'feedbacks.createResult',
  delete: 'feedbacks.deleteResult',
  fetch: 'feedbacks.fetchResult',
}

export const ErrorTypes = {
  create: 'feedbacks.createError',
  delete: 'feedbacks.deleteError',
  fetch: 'feedbacks.fetchError',
}

export class Create implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.create;
  readonly resultType = ResultTypes.create;
  readonly errorType = ErrorTypes.create;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public args = null;
  constructor(public payload: CreatePayload) {}
}

export class Delete implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.delete;
  readonly resultType = ResultTypes.delete;
  readonly errorType = ErrorTypes.delete;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: DeleteArgs) {}
}

export class Fetch implements DefaultActionInterface {
  readonly httpMethod = 'GET';
  readonly type = Types.fetch;
  readonly resultType = ResultTypes.fetch;
  readonly errorType = ErrorTypes.fetch;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: FetchArgs) {}
}
