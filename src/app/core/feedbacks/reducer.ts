import { ResultTypes } from './actions';
import { DefaultResultInterface } from '../common.models';
import { FeedbackModel } from './models';

export interface State {
  feedbacks: FeedbackModel[];
  total: number;
}

export const initialState: State = {
  feedbacks: null,
  total: 0,
};

export function reducer(state = initialState, action: DefaultResultInterface): State {

  switch(action.type) {
    case ResultTypes.fetch:
      return {
        ...state,
        feedbacks: action.payload,
        total: action.total,
      };
  }

  return state;
}
