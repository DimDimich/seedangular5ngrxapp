import { PaginationArgs } from '../common.models';

export interface FeedbackModel {
  id: string;
  ip: string;
  client_id: string;
  created_timestamp: number;
  email: string;
  message: string;
  name: string;
  user_id: string;
  useragent: string;
}

export interface CreatePayload {
}

export interface DeleteArgs {
}

export interface FetchArgs extends PaginationArgs {
}
