import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

export interface PageOptions {
  count: number;
  offset: number;
  total: number;
}

export interface PaginationBtn {
  offset: number;
  text: string;
}

@Component({
  selector: 'paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit {
  @Output() offsetChange = new EventEmitter();
  @Input() hideWhenOnePage: boolean = true;
  @Input() set pageOptions({count, offset, total}: PageOptions) {
    this.count = count;
    this.offset = offset;

    this.current = Math.ceil(offset / count);
    this.totalPages = Math.floor(total / count);

    this.buttons = [];

    let left = this.current - 2;
    let right = this.current + 2;

    if (left < 0) {
      right -= left;
    }

    if (right > this.totalPages) {
      left += (this.totalPages - right);
    }

    this.buttons.push({offset: 0, text: '<<'});

    for(let i = 0; i <= this.totalPages; ++i) {
      if (i >= left && i <= right) {
        this.buttons.push({
          offset: count * i,
          text: `${i + 1}`,
        });
      }
    }

    this.buttons.push({offset: count * this.totalPages, text: '>>'});
  }

  public current: number;
  public offset: number;
  public count: number;
  public totalPages: number = 0;
  public buttons: PaginationBtn[] = [];

  constructor() { }

  ngOnInit() {}
}
