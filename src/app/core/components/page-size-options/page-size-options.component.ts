import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'page-size-options',
  templateUrl: './page-size-options.component.html',
  styleUrls: ['./page-size-options.component.scss']
})
export class PageSizeOptionsComponent implements OnInit {
  @Output() valueChange = new EventEmitter();
  @Input() options: number[] = [10, 20, 50, 100];
  @Input() value = this.options[0];

  constructor() { }

  ngOnInit() {
  }
}
