import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'product-block',
  templateUrl: './product-block.component.html',
  styleUrls: ['./product-block.component.scss']
})
export class ProductBlockComponent implements OnInit {
  @Input() imageUrl = '';

  constructor() { }

  ngOnInit() {
  }

}
