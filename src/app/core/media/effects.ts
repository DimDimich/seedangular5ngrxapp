import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import 'rxjs/add/operator/mergeMap';
import { Types } from './actions';
import { HttpInterceptorImpl } from '../http-interceptor.service';
import { DefaultActionInterface } from '../common.models';
import { flatObjectToArray } from '../common.functions';

@Injectable()
export class Effects {
  constructor(private actions$: Actions, private service: HttpInterceptorImpl) {}

  @Effect()
  httpEffects = this.actions$
  .ofType(...flatObjectToArray(Types))
  .mergeMap((action: DefaultActionInterface) => this.service.request(action));
}
