export enum FileTargets {
  product = 'product',
  banner = 'xprop/banner',
};

export interface UploadProductImageArgs {
  target: 'product';
  product_id: string;
}

export interface UploadXPropBannerImageArgs {
  target: 'xprop/banner';
  prop_id: string;
}

export type UploadImageArgs = UploadProductImageArgs | UploadXPropBannerImageArgs;
