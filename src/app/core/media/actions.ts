import { DefaultActionInterface } from '../common.models';
import { UploadImageArgs } from './models';

export const Types = {
  uploadImage: 'media.uploadImage',
}

export const ResultTypes = {
  uploadImage: 'media.uploadImageResult',
}

export const ErrorTypes = {
  uploadImage: 'media.uploadImageError',
}

export class UploadImage implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.uploadImage;
  readonly resultType = ResultTypes.uploadImage;
  readonly errorType = ErrorTypes.uploadImage;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload: FormData;

  constructor(public args: UploadImageArgs, file: any) {
    this.payload = new FormData();
    this.payload.append('file', file);
  }
}
