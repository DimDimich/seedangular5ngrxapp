import { PaginationArgs } from './common.models';

export const PaginationDefaults: PaginationArgs = {
  count: 10,
  offset: 0,
};
