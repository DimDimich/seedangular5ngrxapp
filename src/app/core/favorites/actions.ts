import { DefaultActionInterface } from '../common.models';
import { ToggleArgs, FetchArgs } from './models';

export const Types = {
  toggle: 'productrels.toggle',
  fetch: 'productrels.fetch',
}

export const ResultTypes = {
  toggle: 'productrels.toggleResult',
  fetch: 'productrels.fetchResult',
}

export const ErrorTypes = {
  toggle: 'productrels.toggleError',
  fetch: 'productrels.fetchError',
}

export class Toggle implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.toggle;
  readonly resultType = ResultTypes.toggle;
  readonly errorType = ErrorTypes.toggle;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public args = null;
  constructor(public payload: ToggleArgs) {}
}

export class Fetch implements DefaultActionInterface {
  readonly httpMethod = 'GET';
  readonly type = Types.fetch;
  readonly resultType = ResultTypes.fetch;
  readonly errorType = ErrorTypes.fetch;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: FetchArgs) {}
}
