import { DefaultActionInterface } from '../common.models';
import { FetchArgs, UpdatePayload, GetArgs } from './models';

export const Types = {
  put: 'documents.update',
  get: 'documents.get',
  fetch: 'documents.fetch',
}

export const ResultTypes = {
  update: 'documents.updateResult',
  get: 'documents.getResult',
  fetch: 'documents.fetchResult',
}

export const ErrorTypes = {
  update: 'documents.updateError',
  get: 'documents.getError',
  fetch: 'documents.fetchError',
}

export class Update implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.put;
  readonly resultType = ResultTypes.update;
  readonly errorType = ErrorTypes.update;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public args = null;
  constructor(public payload: UpdatePayload) {}
}

export class Get implements DefaultActionInterface {
  readonly httpMethod = 'GET';
  readonly type = Types.get;
  readonly resultType = ResultTypes.get;
  readonly errorType = ErrorTypes.get;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: GetArgs) {}
}

export class Fetch implements DefaultActionInterface {
  readonly httpMethod = 'GET';
  readonly type = Types.fetch;
  readonly resultType = ResultTypes.fetch;
  readonly errorType = ErrorTypes.fetch;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: FetchArgs) {}
}
