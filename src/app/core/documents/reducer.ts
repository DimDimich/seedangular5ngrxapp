import { ResultTypes } from './actions';
import { DefaultResultInterface } from '../common.models';
import { DocumentModel } from './models';

export interface State {
  documents: DocumentModel[];
  total: number;
}

export const initialState: State = {
  documents: null,
  total: 0,
};

export function reducer(state = initialState, action: DefaultResultInterface): State {

  switch(action.type) {
    case ResultTypes.fetch:
      return {
        ...state,
        documents: action.payload,
        total: action.total,
      };
  }

  return state;
}
