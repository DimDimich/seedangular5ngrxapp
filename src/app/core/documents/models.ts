import { PaginationArgs } from '../common.models';

export interface DocumentModel {
  id: string;
  key: string;
  location: string[];
  mime: string;
  payload: string;
  sort: number;
  version: string;
}

export interface UpdatePayload {
}

export interface DeletePayload {
}

export interface GetArgs {
}

export interface FetchArgs extends PaginationArgs {
}
