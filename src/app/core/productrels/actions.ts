import { DefaultActionInterface } from '../common.models';
import { AddPayload, DeletePayload, FetchArgs } from './models';

export const Types = {
  add: 'productrels.add',
  delete: 'productrels.delete',
  fetch: 'productrels.fetch',
}

export const ResultTypes = {
  add: 'productrels.addResult',
  delete: 'productrels.deleteResult',
  fetch: 'productrels.fetchResult',
}

export const ErrorTypes = {
  add: 'productrels.addError',
  delete: 'productrels.deleteError',
  fetch: 'productrels.fetchError',
}

export class Add implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.add;
  readonly resultType = ResultTypes.add;
  readonly errorType = ErrorTypes.add;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public args = null;
  constructor(public payload: AddPayload) {}
}

export class Delete implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.delete;
  readonly resultType = ResultTypes.delete;
  readonly errorType = ErrorTypes.delete;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public args = null;
  constructor(public payload: DeletePayload) {}
}

export class Fetch implements DefaultActionInterface {
  readonly httpMethod = 'GET';
  readonly type = Types.fetch;
  readonly resultType = ResultTypes.fetch;
  readonly errorType = ErrorTypes.fetch;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: FetchArgs) {}
}
