import { ResultTypes } from './actions';
import { DefaultResultInterface } from '../common.models';
import { ProductModel } from '../products/models';

export interface State {
  productrels: ProductModel[];
  total: number;
}

export const initialState: State = {
  productrels: null,
  total: 0,
};

export function reducer(state = initialState, action: DefaultResultInterface): State {

  switch(action.type) {
    case ResultTypes.fetch:
      return {
        ...state,
        productrels: action.payload,
        total: action.total,
      };
  }

  return state;
}
