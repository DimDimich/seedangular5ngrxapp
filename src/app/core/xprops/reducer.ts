import { ResultTypes } from './actions';
import { PropertyModel } from './models';
import { DefaultResultInterface } from '../common.models';

export interface State {
  selectedProperty: PropertyModel;
  properties: PropertyModel[];
  total: number;
}

export const initialState: State = {
  selectedProperty: null,
  properties: null,
  total: 0,
};

export function reducer(state = initialState, action: DefaultResultInterface): State {

  switch(action.type) {
    case ResultTypes.create:
      return {
        ...state,
        selectedProperty: action.payload[0],
      };
    case ResultTypes.get:
      return {
        ...state,
        selectedProperty: action.payload[0],
      };
    case ResultTypes.fetch:
      return {
        ...state,
        properties: action.payload,
        total: action.total,
      };
  }

  return state;
}
