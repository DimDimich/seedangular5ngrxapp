import { PaginationArgs } from '../common.models';

export interface CreatePayload {
  name: string;
  location: string[];
  order: number;
  info: object;
}

export interface UpdatePayload {
  name: string;
  location: string[];
  order: number;
  info: object;
}

export interface UpdateArgs {
  prop_id: string;
}

export interface DeleteArgs {
  prop_id: string;
}

export interface FetchArgs extends PaginationArgs {
  location?: string;
  groupdepth?: number;
}

export interface GetArgs {
  prop_id: string;
}

export interface MoveArgs {
  prop_id: string;
  location: string[];
}

export interface PropertyModel {
  id: string;
  image_id: string;
  name: string;
  location: string[];
  order: number;
  info: object;
}


