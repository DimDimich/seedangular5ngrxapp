import { DefaultActionInterface } from '../common.models';
import {
  CreatePayload,
  UpdatePayload,
  UpdateArgs,
  DeleteArgs,
  FetchArgs,
  GetArgs,
  MoveArgs } from './models';

export const Types = {
  create: 'xprops.create',
  get: 'xprops.get',
  update: 'xprops.update',
  delete: 'xprops.delete',
  move: 'xprops.move',
  fetch: 'xprops.fetch'
}

export const ResultTypes = {
  create: 'xprops.createResult',
  get: 'xprops.getResult',
  update: 'xprops.updateResult',
  delete: 'xprops.deleteResult',
  move: 'xprops.moveResult',
  fetch: 'xprops.fetchResult'
}

export const ErrorTypes = {
  create: 'xprops.createError',
  get: 'xprops.getError',
  update: 'xprops.updateError',
  delete: 'xprops.deleteError',
  move: 'xprops.moveError',
  fetch: 'xprops.fetchError'
}

export class Create implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.create;
  readonly resultType = ResultTypes.create;
  readonly errorType = ErrorTypes.create;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public args = null;
  constructor(public payload: CreatePayload) {}
}

export class Get implements DefaultActionInterface {
  readonly httpMethod = 'GET';
  readonly type = Types.get;
  readonly resultType = ResultTypes.get;
  readonly errorType = ErrorTypes.get;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: GetArgs) {}
}

export class Update implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.update;
  readonly resultType = ResultTypes.update;
  readonly errorType = ErrorTypes.update;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  constructor(public args: UpdateArgs, public payload: UpdatePayload) {}
}

export class Delete implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.delete;
  readonly resultType = ResultTypes.delete;
  readonly errorType = ErrorTypes.delete;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: DeleteArgs) {}
}

export class Move implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.move;
  readonly resultType = ResultTypes.move;
  readonly errorType = ErrorTypes.move;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: MoveArgs) {}
}

export class Fetch implements DefaultActionInterface {
  readonly httpMethod = 'GET';
  readonly type = Types.fetch;
  readonly resultType = ResultTypes.fetch;
  readonly errorType = ErrorTypes.fetch;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: FetchArgs) {}
}


