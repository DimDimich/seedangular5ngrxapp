import { ResultTypes, IncomingTypes } from './actions';
import { CrawlerModel } from './models';
import { DefaultResultInterface } from '../common.models';

export interface State {
  crawlers: CrawlerModel[];
  total: number;
}

export const initialState: State = {
  crawlers: null,
  total: 0,
};

export function reducer(state = initialState, action: DefaultResultInterface): State {
  switch(action.type) {
    case ResultTypes.fetch:
      return {
        ...state,
        crawlers: action.payload,
        total: action.total,
      };
    case IncomingTypes.CrawlerStopped:
    case IncomingTypes.CrawlerInProgress:
      return {
        ...state,
        crawlers: updateCrawler(state.crawlers, action.payload[0]),
      };
  }

  return state;
}

function updateCrawler(crawlers: CrawlerModel[], crawler: CrawlerModel) {
  let index = crawlers.findIndex(c => c.id === crawler.id);
  crawlers[index] = crawler;
  return crawlers;
}

