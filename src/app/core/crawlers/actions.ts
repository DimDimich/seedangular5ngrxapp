import { DefaultActionInterface } from '../common.models';
import { FetchArgs, StartArgs, StopArgs } from './models';

export const IncomingTypes = {
  CrawlerInProgress: 'CrawlerInProgress',
  CrawlerStopped: 'CrawlerStopped',
}

export const Types = {
  fetch: 'crawlers.fetch',
  start: 'crawlers.start',
  stop: 'crawlers.stop'
}

export const ResultTypes = {
  fetch: 'crawlers.fetchResult',
  start: 'crawlers.startResult',
  stop: 'crawlers.stopResult'
}

export const ErrorTypes = {
  fetch: 'crawlers.fetchError',
  start: 'crawlers.startError',
  stop: 'crawlers.stopError'
}

export class Fetch implements DefaultActionInterface {
  readonly httpMethod = 'GET';
  readonly type = Types.fetch;
  readonly resultType = ResultTypes.fetch;
  readonly errorType = ErrorTypes.fetch;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: FetchArgs) {}
}

export class Start implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.start;
  readonly resultType = ResultTypes.start;
  readonly errorType = ErrorTypes.start;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: StartArgs) {}
}

export class Stop implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.stop;
  readonly resultType = ResultTypes.stop;
  readonly errorType = ErrorTypes.stop;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: StopArgs) {}
}

