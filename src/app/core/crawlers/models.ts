import { PaginationArgs } from '../common.models';


export interface FetchArgs extends PaginationArgs {
}

export interface StartArgs {
  crawler_id: string;
}

export interface StopArgs {
  crawler_id: string;
}

export interface CrawlerModel {
  id: string;
  name: string;
  domain: string;
  sort: number;
  collected_now: number;
  in_progress: boolean;
}
