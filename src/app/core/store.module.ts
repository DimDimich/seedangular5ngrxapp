import {
  Store,
  ActionReducerMap,
  MetaReducer,
  StoreModule,
} from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { NgModule } from '@angular/core';
import { environment } from '../../environments/environment';

import * as account from './account';
import * as crawlers from './crawlers';
import * as crawlersrepo from './crawlersrepo';
import * as documents from './documents';
import * as favorites from './favorites';
import * as feedbacks from './feedbacks';
import * as media from './media';
import * as configs from './configs';
import * as productrels from './productrels';
import * as products from './products';
import * as users from './users';
import * as xprops from './xprops';
import * as router from './router';
import { HttpInterceptorImpl } from './http-interceptor.service';
import { SSEimpl } from './sse.service';

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];

export interface State {
  ACCOUNT: account.State,
  CRAWLERS: crawlers.State,
  CRAWLERS_REPO: crawlersrepo.State,
  DOCUMENTS: documents.State,
  FAVORITES: favorites.State,
  FEEDBACKS: feedbacks.State,
  MEDIA: media.State,
  CONFIGS: configs.State,
  PRODUCTRELS: productrels.State,
  PRODUCTS: products.State,
  USERS: users.State,
  XPROPS: xprops.State,
  ROUTER: router.State,
}

export const Reducers: ActionReducerMap<State> = {
  ACCOUNT: account.reducer,
  CRAWLERS: crawlers.reducer,
  CRAWLERS_REPO: crawlersrepo.reducer,
  DOCUMENTS: documents.reducer,
  FAVORITES: favorites.reducer,
  FEEDBACKS: feedbacks.reducer,
  MEDIA: media.reducer,
  CONFIGS: configs.reducer,
  PRODUCTRELS: productrels.reducer,
  PRODUCTS: products.reducer,
  USERS: users.reducer,
  XPROPS: xprops.reducer,
  ROUTER: router.reducer,
};

export const Effects = [
  account.Effects,
  crawlers.Effects,
  crawlersrepo.Effects,
  documents.Effects,
  favorites.Effects,
  feedbacks.Effects,
  media.Effects,
  configs.Effects,
  productrels.Effects,
  products.Effects,
  users.Effects,
  xprops.Effects,
  router.Effects,
];


@NgModule({
  imports: [
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    StoreModule.forRoot(Reducers, { metaReducers }),
    EffectsModule.forRoot(Effects),
  ],
  providers: [
    HttpInterceptorImpl,
    SSEimpl,
    ...Effects,
  ]
})
export class AppStoreModule {
  constructor(public store: Store<State>) {
    this.store.dispatch(new account.Me());
  }
}
