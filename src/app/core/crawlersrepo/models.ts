import { PaginationArgs } from '../common.models';


export interface FetchArgs extends PaginationArgs {
  section?: 'stash' | 'ban';
  crawler_id: string;
}

export interface BanArgs {
  digest_id: string;
}

export interface UnbanArgs {
  digest_id: string;
  to_stash: string;
}

export interface DeleteArgs {
  digest_id: string;
}

export interface MetaProductModel {
  digest_id: string;
  crawler_id: string;
  image_id: string;
  name: string;
  article: string;
  sizes: string;
  domain: string;
  timestamp: number;
  stash: boolean;
  ban: boolean;
}

