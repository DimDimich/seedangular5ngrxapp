import { DefaultActionInterface } from '../common.models';
import { FetchArgs, BanArgs, UnbanArgs, DeleteArgs } from './models';

export const Types = {
  fetch: 'crawlersrepo.fetch',
  ban: 'crawlersrepo.ban',
  unban: 'crawlersrepo.unban',
  delete: 'crawlersrepo.delete'
}

export const ResultTypes = {
  fetch: 'crawlersrepo.fetchResult',
  ban: 'crawlersrepo.banResult',
  unban: 'crawlersrepo.unbanResult',
  delete: 'crawlersrepo.deleteResult'
}

export const ErrorTypes = {
  fetch: 'crawlersrepo.fetchError',
  ban: 'crawlersrepo.banError',
  unban: 'crawlersrepo.unbanError',
  delete: 'crawlersrepo.deleteError'
}

export class Fetch implements DefaultActionInterface {
  readonly httpMethod = 'GET';
  readonly type = Types.fetch;
  readonly resultType = ResultTypes.fetch;
  readonly errorType = ErrorTypes.fetch;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: FetchArgs) {}
}

export class Ban implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.ban;
  readonly resultType = ResultTypes.ban;
  readonly errorType = ErrorTypes.ban;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: BanArgs) {}
}

export class Unban implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.unban;
  readonly resultType = ResultTypes.unban;
  readonly errorType = ErrorTypes.unban;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: UnbanArgs) {}
}

export class Delete implements DefaultActionInterface {
  readonly httpMethod = 'POST';
  readonly type = Types.delete;
  readonly resultType = ResultTypes.delete;
  readonly errorType = ErrorTypes.delete;
  readonly actionsOnSuccess = [];
  readonly actionsOnError = [];
  public payload = null;
  constructor(public args: DeleteArgs) {}
}

