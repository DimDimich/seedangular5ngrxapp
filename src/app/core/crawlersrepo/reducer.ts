import { ResultTypes } from './actions';
import { MetaProductModel } from './models';
import { DefaultResultInterface } from '../common.models';

export interface State {
  metaproducts: MetaProductModel[];
  total: number;
}

export const initialState: State = {
  metaproducts: null,
  total: 0,
};

export function reducer(state = initialState, action: DefaultResultInterface): State {

  switch(action.type) {
    case ResultTypes.fetch:
      return {
        ...state,
        metaproducts: action.payload,
        total: action.total,
      };
  }

  return state;
}
