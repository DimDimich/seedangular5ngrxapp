import { Action } from '@ngrx/store';

// DEFAULT ACTION INTERFACES
export interface DefaultActionInterface extends Action {
  resultType?: string;
  errorType?: string;
  actionsOnSuccess?: any[];
  actionsOnError?: any[];
  httpMethod?: 'GET' | 'POST';
  args: any;
  payload: any;
}

export interface DefaultResultInterface extends Action {
  authorization: boolean;
  authorization_expires: number;
  payload: any[];
  total: number;
  description: string;
  error: string;
  error_cause: string;
  event: string;
}

export interface DefaultServerSentEvent {

}

export interface PaginationArgs {
  count: number;
  offset: number;
}
