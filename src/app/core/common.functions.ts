import { PaginationArgs } from './common.models';
import { PaginationDefaults } from './common.constants';
export function flatObjectToArray(obj: any): any[] {
  const result = [];
  Object.getOwnPropertyNames(obj).forEach(name => {
    result.push(obj[name]);
  });
  return result;
}


export function getCountOffsetFromQueryParams(query: any, defaultCount: number = PaginationDefaults.count, defaultOffset: number = PaginationDefaults.offset): PaginationArgs {
  let count = parseInt(query.count);
  let offset = parseInt(query.offset);

  if (count === NaN || count === null || count === undefined) {
    count = defaultCount;
  }

  if (offset === NaN || offset === null || offset === undefined) {
    offset = defaultOffset;
  }

  return { count, offset };
}

export function extractQueryParams(query: any, seed: {[key: string]: string | number}) {
  const keys = Object.getOwnPropertyNames(seed);
  const result = {};
  keys.forEach(key => {
    if (query[key]) {
      switch (typeof seed[key]) {
        case 'number':
          let v = parseInt(query[key], 10);
          result[key] = v === NaN ? seed[key] : v;
          break;
        case 'string':
          result[key] = query[key];
          break;
      }
    }
  });

  return result;
}
