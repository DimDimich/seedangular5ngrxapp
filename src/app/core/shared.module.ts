import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { PaginatorComponent } from './components/paginator/paginator.component';
import { PageSizeOptionsComponent } from './components/page-size-options/page-size-options.component';
import { AuthGuard } from './guards/auth';
import { ProductBlockComponent } from './components/product-block/product-block.component';

@NgModule({
  declarations: [
    PaginatorComponent,
    PageSizeOptionsComponent,
    ProductBlockComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    PaginatorComponent,
    PageSizeOptionsComponent,
    ProductBlockComponent,
  ],
  providers: [AuthGuard]
})
export class SharedModule {
}
