import { Injectable, NgZone } from '@angular/core';
import { environment } from '../../environments/environment';
import { Store } from '@ngrx/store';
import * as AppStore from './store.module';
import { of } from 'rxjs/observable/of';
import 'eventsource-polyfill';
import 'rxjs/add/operator/mergeMap';

export interface SSEinterface {
  connect: (sourceUrl: string) => void;
  disconnect: () => void;
}

@Injectable()
export class SSEimpl implements SSEinterface {
  readonly defaultOptions = { withCredentials: true };
  private sse: EventSource;

  constructor(
    private store: Store<AppStore.State>,
    private zone: NgZone
  ) {}

  connect(sourceUrl: string) {
    if (!this.sse) {
      this.sse = new EventSource(`${environment.api_origin}/${sourceUrl}`, this.defaultOptions);
      this.sse.onmessage = message => {
        let json = JSON.parse(message.data);
        if (json && json.event) {
          // Necessary to run it inside zone, to allow change detection work with this data
          this.zone.run((() => {
            this.store.dispatch({
              type: json.event,
              payload: json.payload
            });
          }));
        }
      };

      this.sse.onerror = e => {
        console.log(e);
        if (this.sse.CONNECTING) {
          // Connection problems
          // this.store.dispatch();
          console.log('Connecting');
        } else {
          // Connection closed
          // this.store.dispatch();
          console.log('Closed');
        }
      };
    }

    return of({type: 'sse service attempts to connect'});
  }

  disconnect() {
    if (this.sse) {
      this.sse.close();
    }
  }
}
