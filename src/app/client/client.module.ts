import { NgModule } from '@angular/core';
import { SharedModule } from '../core/shared.module';
import { ClientRoutingModule } from './client-routing.module';
import { ClientComponent } from './client.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  imports: [
    SharedModule,
    ClientRoutingModule,
  ],
  declarations: [
    ClientComponent,
    HeaderComponent,
    FooterComponent,
  ],
})
export class ClientModule {}
