import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogPageComponent } from './catalog-page.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{path: '', component: CatalogPageComponent}]),
  ],
  declarations: [CatalogPageComponent]
})
export class CatalogPageModule { }
