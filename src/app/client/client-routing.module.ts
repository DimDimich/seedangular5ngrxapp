import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientComponent } from './client.component';

const routes: Routes = [
  {
    path: '',
    component: ClientComponent,
    children: [
      {path: '', loadChildren: 'app/client/home-page/home-page.module#HomePageModule'},
      {path: 'catalog', loadChildren: 'app/client/catalog-page/catalog-page.module#CatalogPageModule'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule {}
