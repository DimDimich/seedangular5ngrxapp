import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './home-page.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../core/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{path: '', component: HomePageComponent}]),
  ],
  declarations: [HomePageComponent]
})
export class HomePageModule { }
