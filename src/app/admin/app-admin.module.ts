import { NgModule } from '@angular/core';
import { AppAdminRoutingModule } from './app-admin-routing.module';
import { SharedModule } from '../core/shared.module';
import { AppAdminComponent } from './app-admin.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { AdminFooterComponent } from './admin-footer/admin-footer.component';
import { AdminNavbarComponent } from './admin-navbar/admin-navbar.component';
import { AdminProgressbarComponent } from './admin-progressbar/admin-progressbar.component';
import { UsersPageComponent } from './users-page/users-page.component';
import { UserDetailsPageComponent } from './user-details-page/user-details-page.component';
import { ProductsPageComponent } from './products-page/products-page.component';
import { CrawlersPageComponent } from './crawlers-page/crawlers-page.component';
import { CrawlersRepoPageComponent } from './crawlers-repo-page/crawlers-repo-page.component';
import { DocumentsPageComponent } from './documents-page/documents-page.component';
import { ConfigsPageComponent } from './configs-page/configs-page.component';
import { XpropsPageComponent } from './xprops-page/xprops-page.component';
import { FeedbacksPageComponent } from './feedbacks-page/feedbacks-page.component';
import { MediaContentPageComponent } from './media-content-page/media-content-page.component';
import { DocumentDetailsPageComponent } from './document-details-page/document-details-page.component';
import { ProductDetailsPageComponent } from './product-details-page/product-details-page.component';

@NgModule({
  imports: [
    SharedModule,
    AppAdminRoutingModule,
  ],
  providers: [],
  declarations: [
    AppAdminComponent,
    LoginPageComponent,
    AdminFooterComponent,
    AdminNavbarComponent,
    AdminProgressbarComponent,
    UsersPageComponent,
    UserDetailsPageComponent,
    ProductsPageComponent,
    CrawlersPageComponent,
    CrawlersRepoPageComponent,
    DocumentsPageComponent,
    ConfigsPageComponent,
    XpropsPageComponent,
    FeedbacksPageComponent,
    MediaContentPageComponent,
    DocumentDetailsPageComponent,
    ProductDetailsPageComponent,
  ]
})
export class AppAdminModule {}
