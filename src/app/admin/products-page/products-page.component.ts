import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import * as AppStore from '../../core/store.module';
import * as Products from '../../core/products';
import { ActivatedRoute, Router } from '@angular/router';
import { PaginationDefaults } from '../../core/common.constants';
import { extractQueryParams } from '../../core/common.functions';

@Component({
  selector: 'products-page',
  templateUrl: './products-page.component.html',
  styleUrls: ['./products-page.component.scss']
})
export class ProductsPageComponent implements OnInit {
  public productsState$: Observable<Products.State>;

  public pageSizeOptions = [10, 20, 50, 100];

  private _searchParams: Products.FetchByFilterInputArgs = PaginationDefaults;
  setSearchParams(value) {
    Object.assign(this._searchParams, value);
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {...this._searchParams, ts: Date.now()},
    });
  };
  get searchParams() {
    return this._searchParams;
  };

  constructor(
    public store: Store<AppStore.State>,
    public route: ActivatedRoute,
    public router: Router,
  ) { }

  ngOnInit() {
    this.productsState$ = this.store.select(state => state.PRODUCTS);
    this.resetFilters();

    this.route.queryParams.subscribe(q => {
      Object.assign(this._searchParams, extractQueryParams(q, {
        ...PaginationDefaults, q: '', prop_ids: ''
      }))
      this.store.dispatch(new Products.FetchByFilter({...this._searchParams, prefix: 'yes'}));
    });
  }

  resetFilters() {
    this._searchParams = {...PaginationDefaults};
  }

  delete(product_id) {
    if (confirm('Confirm action')) {
      const deleteAction = new Products.Delete({product_id});
      deleteAction.actionsOnSuccess.push(new Products.Fetch(this._searchParams));
      this.store.dispatch(deleteAction);
    }
  }
}
