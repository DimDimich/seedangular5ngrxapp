import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import * as AppStore from '../../core/store.module';
import * as Users from '../../core/users';
import { FetchArgs } from '../../core/users';
import { ActivatedRoute, Router } from '@angular/router';
import { PaginationDefaults } from '../../core/common.constants';
import { extractQueryParams } from '../../core/common.functions';

@Component({
  selector: 'users-page',
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.scss']
})
export class UsersPageComponent implements OnInit {
  public usersState$: Observable<Users.State>;

  public pageSizeOptions = [10, 20, 50, 100];

  private _searchParams: FetchArgs;
  setSearchParams(value) {
    Object.assign(this._searchParams, value);
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {...this._searchParams, ts: Date.now()},
    });
  };
  get searchParams() {
    return this._searchParams;
  };

  constructor(
    public store: Store<AppStore.State>,
    public route: ActivatedRoute,
    public router: Router,
  ) { }

  ngOnInit() {
    this.usersState$ = this.store.select(state => state.USERS);
    this.resetFilters();

    this.route.queryParams.subscribe(q => {
      Object.assign(this._searchParams, extractQueryParams(q, {
        ...PaginationDefaults, login: '', name: '', email: ''
      }))
      this.store.dispatch(new Users.FetchByFilter(this._searchParams));
    });
  }

  resetFilters() {
    this._searchParams = {...PaginationDefaults};
  }

  delete(user_id) {
    if (confirm('Confirm action')) {
      const deleteAction = new Users.Delete({user_id});
      deleteAction.actionsOnSuccess.push(new Users.FetchByFilter(this._searchParams));
      this.store.dispatch(deleteAction);
    }
  }
}
