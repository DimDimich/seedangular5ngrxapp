import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import * as AppStore from '../../core/store.module';
import * as Users from '../../core/users';
import { Subscription } from 'rxjs/Subscription';
import { skipWhile } from 'rxjs/operators';

@Component({
  selector: 'user-details-page',
  templateUrl: './user-details-page.component.html',
  styleUrls: ['./user-details-page.component.scss']
})
export class UserDetailsPageComponent implements OnInit, OnDestroy {
  private subs: Subscription[];

  public form: FormGroup;
  public user$: Observable<Users.UserModel>;
  public roles$: Observable<Users.RoleModel[]>;
  public user_id: string;

  constructor(
    public store: Store<AppStore.State>,
    public route: ActivatedRoute,
    public router: Router,
  ) { }

  ngOnInit() {
    this.subs = [];
    this.resetForm();
    this.user_id = null;

    this.user$ = this.store.select(state => state.USERS.selectedUser);
    this.roles$ = this.store.select(state => state.USERS.roles);

    this.store.dispatch(new Users.Roles());

    this.subs.push(this.route.queryParams.subscribe(q => {
      if (q.user_id) {
        this.store.dispatch(new Users.Get({ user_id: q.user_id }));
      }
    }));

    this.subs.push(this.user$
      .pipe(skipWhile(value => value === null))
      .subscribe(user => {
        this.user_id = user.id;
        this.form = Users.generateForm(user);
      }
    ));
  }

  ngOnDestroy() {
    this.subs.forEach(s => s.unsubscribe());
    this.subs = [];
    this.store.dispatch(new Users.ResetFields({
      selectedUser: true
    }));
  }

  resetForm() {
    this.form = Users.generateForm();
  }

  submitForm() {
    if (this.user_id) {
      this.store.dispatch(new Users.Update({user_id: this.user_id}, this.form.value));
    } else {
      this.store.dispatch(new Users.Create(this.form.value));
    }
  }

  delete() {
    this.store.dispatch(new Users.Delete({user_id: this.user_id}));
  }
}
