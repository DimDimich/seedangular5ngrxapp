import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import * as AppStore from '../../core/store.module';
import * as Products from '../../core/products';
import { Subscription } from 'rxjs/Subscription';
import { skipWhile } from 'rxjs/operators';

@Component({
  selector: 'product-details-page',
  templateUrl: './product-details-page.component.html',
  styleUrls: ['./product-details-page.component.scss']
})
export class ProductDetailsPageComponent implements OnInit {
  private subs: Subscription[];

  public form: FormGroup;
  public product$: Observable<Products.ProductModel>;
  public product_id: string;

  constructor(
    public store: Store<AppStore.State>,
    public route: ActivatedRoute,
    public router: Router,
  ) { }

  ngOnInit() {
    this.subs = [];
    this.resetForm();
    this.product_id = null;

    this.product$ = this.store.select(state => state.PRODUCTS.selectedProduct);

    this.subs.push(this.route.queryParams.subscribe(q => {
      if (q.user_id) {
        this.store.dispatch(new Products.Get({product_id: q.product_id}));
      }
    }));

    this.subs.push(this.product$
      .pipe(skipWhile(value => value === null))
      .subscribe(product => {
        this.product_id = product.id;
        this.form = Products.generateForm(product);
      }
    ));
  }

  ngOnDestroy() {
    this.subs.forEach(s => s.unsubscribe());
    this.subs = [];
    this.store.dispatch(new Products.ResetFields({
      selectedProduct: true
    }));
  }

  resetForm() {
    this.form = Products.generateForm();
  }

  submitForm() {
    if (this.product_id) {
      this.store.dispatch(new Products.Update({product_id: this.product_id}, this.form.value));
    } else {
      this.store.dispatch(new Products.Create(this.form.value));
    }
  }

  delete() {
    this.store.dispatch(new Products.Delete({product_id: this.product_id}));
  }
}
