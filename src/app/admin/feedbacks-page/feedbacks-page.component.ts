import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import * as AppStore from '../../core/store.module';
import * as Feedbacks from '../../core/feedbacks';
import { FetchArgs } from '../../core/feedbacks';
import { ActivatedRoute, Router } from '@angular/router';
import { PaginationDefaults } from '../../core/common.constants';
import { extractQueryParams } from '../../core/common.functions';

@Component({
  selector: 'feedbacks-page',
  templateUrl: './feedbacks-page.component.html',
  styleUrls: ['./feedbacks-page.component.scss']
})
export class FeedbacksPageComponent implements OnInit {
  public state$: Observable<Feedbacks.State>;

  public pageSizeOptions = [10, 20, 50, 100];

  private _searchParams: FetchArgs;
  setSearchParams(value) {
    Object.assign(this._searchParams, value);
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {...this._searchParams, ts: Date.now()},
    });
  };
  get searchParams() {
    return this._searchParams;
  };

  constructor(
    public store: Store<AppStore.State>,
    public route: ActivatedRoute,
    public router: Router,
  ) { }

  ngOnInit() {
    this.state$ = this.store.select(state => state.FEEDBACKS);
    this.resetFilters();

    this.route.queryParams.subscribe(q => {
      Object.assign(this._searchParams, extractQueryParams(q, {
        ...PaginationDefaults
      }))
      this.store.dispatch(new Feedbacks.Fetch(this._searchParams));
    });
  }

  resetFilters() {
    this._searchParams = {...PaginationDefaults};
  }
}
