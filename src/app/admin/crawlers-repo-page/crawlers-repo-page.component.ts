import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import * as AppStore from '../../core/store.module';
import * as CrawlersRepo from '../../core/crawlersrepo';
import { Subscription } from 'rxjs/Subscription';
import { skipWhile } from 'rxjs/operators';
import { PaginationDefaults } from '../../core/common.constants';
import { extractQueryParams } from '../../core/common.functions';

@Component({
  selector: 'crawlers-repo-page',
  templateUrl: './crawlers-repo-page.component.html',
  styleUrls: ['./crawlers-repo-page.component.scss']
})
export class CrawlersRepoPageComponent implements OnInit, OnDestroy {
  private subs: Subscription[];

  public crawlerRepoState$: Observable<CrawlersRepo.State>;
  public crawler_id: string;

  public pageSizeOptions = [10, 20, 50, 100];

  private _searchParams: CrawlersRepo.FetchArgs;
  setSearchParams(value) {
    Object.assign(this._searchParams, value);
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {...this._searchParams, ts: Date.now()},
    });
  };
  get searchParams() {
    return this._searchParams;
  };

  constructor(
    public store: Store<AppStore.State>,
    public route: ActivatedRoute,
    public router: Router,
  ) { }

  ngOnInit() {
    this.subs = [];
    this.resetFilters();

    this.crawlerRepoState$ = this.store.select(state => state.CRAWLERS_REPO);

    this.route.queryParams.subscribe(q => {
      Object.assign(this._searchParams, extractQueryParams(q, {
        ...PaginationDefaults, crawler_id: '', section: 'stash',
      }))
      this.store.dispatch(new CrawlersRepo.Fetch(this._searchParams));
    });
  }

  ngOnDestroy() {
    this.subs.forEach(s => s.unsubscribe());
    this.subs = [];
  }

  resetFilters() {
    this._searchParams = {...PaginationDefaults, crawler_id: ''};
  }
}
