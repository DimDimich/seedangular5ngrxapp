import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as AppStore from '../../core/store.module';
import * as Account from '../../core/account';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  public form: FormGroup;

  constructor(public store: Store<AppStore.State>) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm() {
    this.form = new FormGroup({
      login: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }

  login() {
    this.store.dispatch(new Account.Login(this.form.value));
  }
}
