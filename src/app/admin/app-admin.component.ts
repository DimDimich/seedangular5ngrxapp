import { Component, OnInit, OnDestroy } from '@angular/core';
import { SSEimpl } from '../core/sse.service';

@Component({
  selector: 'app-admin',
  templateUrl: './app-admin.component.html',
  styleUrls: ['./app-admin.component.scss']
})
export class AppAdminComponent implements OnInit, OnDestroy {
  constructor(public sse: SSEimpl) {

  }

  ngOnInit() {
    this.sse.connect('events.source');
  }

  ngOnDestroy() {
    this.sse.disconnect();
  }
}
