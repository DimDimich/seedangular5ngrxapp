import { Component, OnInit, Input } from '@angular/core';
import { HttpInterceptorImpl } from '../../core/http-interceptor.service';

@Component({
  selector: 'admin-progressbar',
  template: `<div class="loader" *ngIf="httpInterceptor.busy"></div>`,
  styleUrls: ['./admin-progressbar.component.scss']
})
export class AdminProgressbarComponent {
  constructor(public httpInterceptor: HttpInterceptorImpl) { }
}
