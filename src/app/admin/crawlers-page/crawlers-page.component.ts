import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import * as AppStore from '../../core/store.module';
import * as Crawlers from '../../core/crawlers';
import { FetchArgs } from '../../core/crawlers';
import { ActivatedRoute, Router } from '@angular/router';
import { PaginationDefaults } from '../../core/common.constants';
import { extractQueryParams } from '../../core/common.functions';

@Component({
  selector: 'crawlers-page',
  templateUrl: './crawlers-page.component.html',
  styleUrls: ['./crawlers-page.component.scss'],
})
export class CrawlersPageComponent implements OnInit {
  public crawlersState$: Observable<Crawlers.State>;

  public pageSizeOptions = [10, 20, 50, 100];

  private _searchParams: FetchArgs = {...PaginationDefaults};
  setSearchParams(value) {
    Object.assign(this._searchParams, value);
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {...this._searchParams, ts: Date.now()},
    });
  };
  get searchParams() {
    return this._searchParams;
  };

  constructor(
    public store: Store<AppStore.State>,
    public route: ActivatedRoute,
    public router: Router,
  ) { }

  ngOnInit() {
    this.crawlersState$ = this.store.select(state => state.CRAWLERS);

    this.route.queryParams.subscribe(q => {
      Object.assign(this._searchParams, extractQueryParams(q, {
        ...PaginationDefaults
      }))
      this.store.dispatch(new Crawlers.Fetch(this._searchParams));
    });
  }

  start(crawler_id) {
    if (confirm('Confirm action')) {
      this.store.dispatch(new Crawlers.Start({ crawler_id }));
    }
  }

  stop(crawler_id) {
    if (confirm('Confirm action')) {
      this.store.dispatch(new Crawlers.Stop({ crawler_id }));
    }
  }
}
