import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import * as AppStore from '../../core/store.module';
import * as Configs from '../../core/configs';
import { ActivatedRoute, Router } from '@angular/router';
import { PaginationDefaults } from '../../core/common.constants';
import { extractQueryParams } from '../../core/common.functions';
import { GetArgs } from '../../core/configs/models';

@Component({
  selector: 'configs-page',
  templateUrl: './configs-page.component.html',
  styleUrls: ['./configs-page.component.scss']
})
export class ConfigsPageComponent implements OnInit {
  public state$: Observable<Configs.State>;

  constructor(
    public store: Store<AppStore.State>,
    public route: ActivatedRoute,
    public router: Router,
  ) { }

  ngOnInit() {
    // this.state$ = this.store.select(state => state.CONFIGS);
    // this.store.dispatch(new Configs.Get());
  }
}
