import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs/Observable";
import { UserModel } from "../../core/account";
import * as AppStore from "../../core/store.module";
import * as Account from "../../core/account";

@Component({
  selector: "admin-navbar",
  templateUrl: "./admin-navbar.component.html",
  styleUrls: ["./admin-navbar.component.scss"]
})
export class AdminNavbarComponent implements OnInit {
  public user$: Observable<UserModel>;

  constructor(public store: Store<AppStore.State>) {}

  ngOnInit() {
    this.user$ = this.store.select(state => state.ACCOUNT.user);
  }

  logout() {
    this.store.dispatch(new Account.Logout());
  }
}
