import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AppAdminComponent } from './app-admin.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { UsersPageComponent } from './users-page/users-page.component';
import { UserDetailsPageComponent } from './user-details-page/user-details-page.component';
import { ProductsPageComponent } from './products-page/products-page.component';
import { CrawlersPageComponent } from './crawlers-page/crawlers-page.component';
import { CrawlersRepoPageComponent } from './crawlers-repo-page/crawlers-repo-page.component';
import { ConfigsPageComponent } from './configs-page/configs-page.component';
import { DocumentsPageComponent } from './documents-page/documents-page.component';
import { XpropsPageComponent } from './xprops-page/xprops-page.component';
import { FeedbacksPageComponent } from './feedbacks-page/feedbacks-page.component';
import { ProductDetailsPageComponent } from './product-details-page/product-details-page.component';

const routes: Routes = [
  {
    path: '',
    component: AppAdminComponent,
    children: [
      {
        path: 'login',
        component: LoginPageComponent,
      },
      {
        path: 'products',
        component: ProductsPageComponent,
      },
      {
        path: 'products/details',
        component: ProductDetailsPageComponent,
      },
      {
        path: 'xprops',
        component: XpropsPageComponent,
      },
      {
        path: 'feedbacks',
        component: FeedbacksPageComponent,
      },
      {
        path: 'crawlers',
        component: CrawlersPageComponent,
      },
      {
        path: 'crawlers/repository',
        component: CrawlersRepoPageComponent,
      },
      {
        path: 'configs',
        component: ConfigsPageComponent,
      },
      {
        path: 'documents',
        component: DocumentsPageComponent,
      },
      {
        path: 'users',
        component: UsersPageComponent,
      },
      {
        path: 'users/details',
        component: UserDetailsPageComponent,
      },
      {
        path: '**',
        redirectTo: 'products',
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppAdminRoutingModule {}
